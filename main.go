package main

import (
	"flag"
	"game"
	"log"
	"mongodb"
	"net/http"
)

var (
	sendSampleDataInDB = flag.Bool("initdb", false, "send some example structures to mongodb")
)

func main() {
	defer mongodb.Close()
	flag.Parse()
	if *sendSampleDataInDB {
		err := mongodb.JobsC.Insert(&game.Job{})
		if err != nil {
			log.Fatal("[ERROR] can't send sample job to db, err:", err)
		}
		err = mongodb.OpportunityC.Insert(&game.Opportunity{Label: "Some label", Options: []game.Option{{}}})
		if err != nil {
			log.Fatal("[ERROR] can't send sample opportunity to db, err:", err)
		}
	}

	http.HandleFunc("/jobs", game.GetJobsHandler)
	http.HandleFunc("/register", game.RegisterGameHandler)
	http.HandleFunc("/next_move", game.NextMove)
	http.HandleFunc("/apply", game.ApplyOption)
	log.Println("[ERROR]: http listen and serve error: ", http.ListenAndServe(":8080", nil))
}
