package game

import (
	"crypto/rand"
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"log"
	"mongodb"
	"net/http"
	"strconv"
)

func getToken() string {
	b := make([]byte, 16)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

// GetJobsHandler отдает текущие профессии
func GetJobsHandler(w http.ResponseWriter, r *http.Request) { // todo: сделать кеш
	if r.Method == http.MethodGet {
		jobs := []*Job{}
		err := mongodb.JobsC.Find(nil).All(&jobs)
		if err != nil {
			log.Println("[NEVER]: error while trying to get all jobs in GetJobsHandler")
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}
		jobsBytes, err := json.Marshal(jobs)
		if err != nil {
			log.Println("[NEVER]: error while marshaling all jobs arr in GetJobsHandler")
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.Write(jobsBytes)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
}

// RegisterGameHandler запрос на начало игры, отправляется id профессии
func RegisterGameHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			log.Println("[ERROR]: can't parse requests form in RegisterGameHandler")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		jobId := r.Form.Get("job_id")
		username := r.Form.Get("username")
		if jobId == "" || username == "" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		// получаем профессию
		var job Job
		err = mongodb.JobsC.FindId(bson.ObjectIdHex(jobId)).One(&job)
		if err != nil {
			log.Printf("[ERROR]: no such job (%s) from RegisterGameHandler, err: %v\n", jobId, err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// теперь создаем новую игру
		g := Game{
			Id:       bson.NewObjectId(),
			JobId:    jobId,
			Token:    getToken(),
			Username: username,
			Step:     0,
			Money:    job.InitMoney,
			Income:   job.Salary,
			Expense:  job.Rent,
		}
		err = mongodb.GamesC.Insert(&g)
		if err != nil {
			log.Fatal("[NEVER] error while inserting new game in RegisterGameHandler, err:", err)
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		response, err := json.Marshal(struct {
			Token  string        `json:"token"`
			GameId bson.ObjectId `json:"game_id"`
		}{Token: g.Token, GameId: g.Id})
		w.Write(response)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// NextMove возвращает новую возможность
func NextMove(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodGet {
		// сейчас нужно провалидировать запрос и получить game
		// три параметра: username, game_id, token
		username := r.URL.Query().Get("username")
		token := r.URL.Query().Get("token")
		gameId := r.URL.Query().Get("game_id")
		var g Game
		err := mongodb.GamesC.FindId(bson.ObjectIdHex(gameId)).One(&g)
		if err != nil {
			log.Println("[ERROR] error while getting game by id in NextMove(), err:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if g.Token != token || g.Username != username {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		var op Opportunity
		err = mongodb.OpportunityC.Pipe([]bson.M{{"$sample": bson.M{"size": 1}}}).One(&op) // извлекаем случайный элемент
		if err != nil {
			log.Println("[NEVER] error making sample of opportunities in NextMove(), err:", err)
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		response, err := json.Marshal(struct {
			Game        Game        `json:"game"`
			Opportunity Opportunity `json:"opportunity"`
		}{g, op})
		w.Write(response)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// ApplyOption позволяет активировать опцию
func ApplyOption(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		err := r.ParseForm()
		if err != nil {
			log.Println("[ERROR]: can't parse requests form in ApplyOption")
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		// сейчас нужно провалидировать запрос и получить game
		// три параметра: username, game_id, token
		username := r.Form.Get("username")
		token := r.Form.Get("token")
		gameId := r.Form.Get("game_id")
		var g Game
		err = mongodb.GamesC.FindId(bson.ObjectIdHex(gameId)).One(&g)
		if err != nil {
			log.Println("[ERROR] error while getting game by id in ApplyOption(), err:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if g.Token != token || g.Username != username {
			w.WriteHeader(http.StatusForbidden)
			return
		}

		opId := r.Form.Get("op_id")
		var op Opportunity
		err = mongodb.OpportunityC.FindId(bson.ObjectIdHex(opId)).One(&op)
		if err != nil {
			log.Println("[ERROR] error while getting opportunity by id in ApplyOption(), err:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		optionIdStr := r.Form.Get("op_option_id")
		optionId, err := strconv.Atoi(optionIdStr)
		if err != nil || optionId < 0 || optionId >= len(op.Options) {
			log.Printf("[ERROR]: invalid op_option_id param (%s (max %d)), err: %v\n", optionIdStr, len(op.Options), err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// нужно прибавить ход и скоректировать деньги
		g.Step += 1
		g.Money += g.Income - g.Expense

		option := op.Options[optionId]
		if option.Income > 0 {
			g.Income += option.Income
		} else {
			g.Expense -= option.Income
		}
		g.Money -= option.Cost

		err = mongodb.GamesC.UpdateId(bson.ObjectIdHex(gameId), bson.M{"$set": g})
		if err != nil {
			log.Println("[NEVER] rewrite insert in ApplyOption err: ", err)
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}

		response, err := json.Marshal(struct {
			Message string `json:"message"`
		}{option.Message})
		if err != nil {
			log.Println("[NEVER] json marshal message error:", err)
			w.WriteHeader(http.StatusServiceUnavailable)
			return
		}
		w.Write(response)
	} else {
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
