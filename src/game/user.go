package game

import (
	"gopkg.in/mgo.v2/bson"
)

// Job выбирается в самом начале игры игроком
type Job struct {
	Id        bson.ObjectId `json:"id" bson:"_id,omitempty"`      // id в базе
	Label     string        `json:"label" bson:"label"`           // название профессии
	Story     string        `json:"story" bson:"story"`           // некоторое описание профессии
	Emoji     string        `json:"emoji" bson:"emoji"`           // значок емодзи для отображения
	Salary    int64         `json:"salary" bson:"salary"`         // зарплата в месяц
	Rent      int64         `json:"rent" bson:"rent"`             // аренда и прочие постоянные расходы
	InitMoney int64         `json:"init_money" bson:"init_money"` // начальные сбережения
}

// Game хранит в себе игру и токен доступа
type Game struct {
	Id       bson.ObjectId `json:"-" bson:"_id,omitempty"` // id игры в базе
	JobId    string        `json:"-" bson:"job_id"`        // id профессии в базе
	Token    string        `json:"-" bson:"token"`         // токен для доступа [a-zA-z0-9]{16}. Токен является id игры
	Username string        `json:"-" bson:"username"`      // логин пользователя, вместе с токеном составляют уникальную пару для доступа к аккаунту
	Step     uint16        `json:"step" bson:"step"`       // номер хода пользователя
	Money    int64         `json:"money" bson:"money"`     // текущее количество денег
	Income   int64         `json:"income" bson:"income"`   // текущий пассивный доход
	Expense  int64         `json:"expense" bson:"expense"` // текущие пассивные расходы
}

type Opportunity struct {
	Id      bson.ObjectId `json:"id" bson:"_id,omitempty"` // id в базе
	Label   string        `json:"label" bson:"label"`      // описание для возможности
	Options []Option      `json:"options" bson:"options"`  // опции для выбора
}

type Option struct {
	Label   string `json:"label" bson:"label"`   // название действия
	Income  int64  `json:"income" bson:"income"` // пассивный доход в месяц
	Cost    int64  `json:"cost" bson:"cost"`     // единовременная стоимость
	Message string `json:"-" bson:"message"`     // сообщение при принятии
}
