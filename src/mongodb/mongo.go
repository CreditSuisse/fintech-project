package mongodb

import (
	"gopkg.in/mgo.v2"
	"log"
)

const ( // todo: сделать через переменные окружения
	mongoCredentials = "localhost:32768"
)

var (
	Session      *mgo.Session // нужно не забыть закрыть сессию
	GameDB       *mgo.Database
	JobsC        *mgo.Collection // todo: сделать приватными
	OpportunityC *mgo.Collection // todo: сделать приватными
	GamesC       *mgo.Collection // todo: сделать приватными
)

func init() {
	var err error
	Session, err = mgo.Dial(mongoCredentials)
	if err != nil {
		log.Fatal("[ERROR] can't dial with server, err:", err)
	}
	Session.SetMode(mgo.Monotonic, true)

	GameDB = Session.DB("game")

	JobsC = GameDB.C("jobs")
	OpportunityC = GameDB.C("opportunities")
	GamesC = GameDB.C("games")
}

func Close() {
	Session.Close()
}
